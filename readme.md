# Language Semantics

Our _"language"_ has five kinds of values and four other kinds of
expressions:

- A **NoPoints** represents the empty set of two-dimensional points.

- A **Point** represents a two-dimensional point with an `x-coordinate`
  and a `y-coordinate`. Both coordinates are floating-point numbers.

- A **Line** is a non-vertical infinite line in the plane, represented by a
  _slope_ and an _intercept_ (as in `y = mx + b` where `m` is the slope and
  `b` is the intercept), both floating-point numbers.

- A **VerticalLine** is an infinite vertical line in the plane,
  represented by its `x-coordinate`.

- A **LineSegment** is a (finite) line segment, represented by the
  `x-` and `y-coordinates` of its endpoints (so four total
  floating-point numbers).

- An **Intersect** expression is not a value. It has two
  subexpressions. The semantics is to evaluate the subexpressions (in
  the same environment) and then return the value that is the
  intersection (in the geometric sense) of the two subresults. For
  example, the intersection of two lines could be one of:

  - `NoPoints`, if the lines are parallel
  - a `Point`, if the lines intersect
  - a `Line`, if the lines have the same slope and intercept (see
  the note below about what we mean by "the same" for floating-point
  numbers)

- A **Let** expression is not a value. It is like let-expressions in
  other languages we have studied: The first subexpression is evaluated
  and the result bound to a variable that is added to the environment
  for evaluating the second subexpression.

- A **Var** expression is not a value. It is for using variables in
  the environment: We look up a string in the environment to get a
  geometric value.

- A **Shift** expression is not a value. It has a _deltaX_ (a
  floating-point number), a _deltaY_ (a floating-point number), and a
  subexpression. The semantics is to evaluate the subexpression and then
  _shift_ the result by `deltaX` (in the x-direction; positive is "to
  the right") and `deltaY` (in the y-direction; positive is
  "up"). More specifically, shifting for each form of value is as follows:

  - `NoPoints` remains `NoPoints`.
  - A `Point` representing `(x,y)` becomes a `Point` representing
  `(x+deltaX, y+deltaY)`.
  - A `Line` with slope `m` and intercept `b` becomes a `Line` with
  slope `m` and an intercept of `b+deltaY-m*deltaX`.
  - A `VerticalLine` becomes a `VerticalLine` shifted by *deltaX*;
  the *deltaY* is irrelevant.
  - A `LineSegment` has its endpoints shift by *deltaX* and *deltaY*.
