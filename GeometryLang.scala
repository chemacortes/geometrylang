package GeometryLang

/*
 * The challenge assignment for homework7 code in scala by Chema CortÃ©s
 * 	
 * a little language for 2D geometry objects
 * 
 * Mixed of OOP and functional styles
 * 
 * each subclass of GeometryExpression, including GeometryValue,
 * can respond to message 'preprocess_prog'
 * by default, the instance is untouched
 * 
 * each subclass of GeometryValue additionally needs:
 * 	shift
 *  
 * Object 'eval_prog' has the role of the eval function, exposing:
 * 
 *  real_close
 *  real_close_point
 *  intersect (only for test purposes)
 *  
 * Note: geometry objects should be immutable: assign to fields only during
 * 		 object construction
 *    
 * Note: For eval_prog, represent environments as a inmutable List
 *
*/

import eval_prog.{ real_close, real_close_point }

trait GeometryExpression {
  def preprocess_prog: GeometryExpression = this
}

trait GeometryValue extends GeometryExpression {
  override def preprocess_prog: GeometryValue = this
  def shift(dx: Double, dy: Double): GeometryValue
}

// Exceptions

class BadProgram(msg: String) extends RuntimeException(msg)
class Impossible(msg: String) extends RuntimeException(msg)

// GeometryValues

object NoPoints extends GeometryValue {
  def shift(dx: Double, dy: Double) = NoPoints
}
case class Point(x: Double, y: Double) extends GeometryValue {
  def shift(dx: Double, dy: Double) = Point(x + dx, y + dy)
}
case class Line(m: Double, b: Double) extends GeometryValue {
  def shift(dx: Double, dy: Double) = Line(m, b + dy - m * dx)
}
case class VerticalLine(x: Double) extends GeometryValue {
  def shift(dx: Double, dy: Double) = VerticalLine(x + dx)
}
case class LineSegment(val x1: Double, val y1: Double, val x2: Double, val y2: Double) extends GeometryValue {
  def shift(dx: Double, dy: Double) = LineSegment(x1 + dx, y1 + dy, x2 + dx, y2 + dy)
  override def preprocess_prog =
    if (real_close_point(x1, y1)(x2, y2))
      Point(x1, y1)
    else if ((real_close(x1, x2) && y1 > y2) || x1 > x2)
      LineSegment(x2, y2, x1, y1)
    else
      this
}

// GeometryExpressions

case class Intersect(e1: GeometryExpression, e2: GeometryExpression) extends GeometryExpression {
  override def preprocess_prog = Intersect(e1.preprocess_prog, e2.preprocess_prog)
}
case class Let(s: String, e1: GeometryExpression, e2: GeometryExpression) extends GeometryExpression {
  override def preprocess_prog = Let(s, e1.preprocess_prog, e2.preprocess_prog)
}
case class Var(s: String) extends GeometryExpression
case class Shift(dx: Double, dy: Double, e: GeometryExpression) extends GeometryExpression {
  override def preprocess_prog = Shift(dx, dy, e.preprocess_prog)
}

// Geometry Eval

object eval_prog {

  private val Epsilon = 0.00001

  def real_close(r1: Double, r2: Double) = Math.abs(r1 - r2) < Epsilon

  def real_close_point(x1: Double, y1: Double)(x2: Double, y2: Double) = real_close(x1, x2) && real_close(y1, y2)

  // two_points_to_line could return a Line or a VerticalLine
  private def two_points_to_line(x1: Double, y1: Double, x2: Double, y2: Double) =
    if (real_close(x1, x2))
      VerticalLine(x1)
    else {
      val m = (y2 - y1) / (x2 - x1)
      val b = y1 - m * x1
      Line(m, b)
    }

  def apply(e: GeometryExpression, env: List[(String, GeometryValue)] = Nil): GeometryValue = e match {
    case v: GeometryValue  => v
    case Intersect(e1, e2) => intersect(eval_prog(e1, env), eval_prog(e2, env))
    case Let(s, e1, e2)    => eval_prog(e2, (s -> eval_prog(e1, env)) :: env)
    case Var(s) => env find (_._1 == s) match {
      case Some((_,v)) => v
      case None    => throw new BadProgram("undefined variable")
    }
    case Shift(dx, dy, e) => eval_prog(e, env).shift(dx, dy)
  }

  def intersect(v1: GeometryValue, v2: GeometryValue): GeometryValue = (v1, v2) match {
    case (NoPoints, _)                        => NoPoints
    case (_, NoPoints)                        => NoPoints

    case (p1 @ Point(x1, y1), Point(x2, y2))  => if (real_close_point(x1, y1)(x2, y2)) p1 else NoPoints
    case (p @ Point(x, y), Line(m, b))        => if (real_close(y, m * x + b)) p else NoPoints
    case (p @ Point(x1, _), VerticalLine(x2)) => if (real_close(x1, x2)) p else NoPoints
    case (_: Point, _: LineSegment)           => intersect(v2, v1)

    case (_: Line, _: Point)                  => intersect(v2, v1)
    case (l @ Line(m1, b1), Line(m2, b2)) =>
      if (real_close(m1, m2))
        if (real_close(b1, b2)) l else NoPoints // some line or parallel lines
      else {
        val x = (b2 - b1) / (m1 - m2)
        val y = m1 * x + b1
        Point(x, y)
      }
    case (Line(m1, b1), VerticalLine(x2))             => Point(x2, m1 * x2 + b1)
    case (_: Line, _: LineSegment)                    => intersect(v2, v1)

    case (_: VerticalLine, _: Point)                  => intersect(v2, v1)
    case (_: VerticalLine, _: Line)                   => intersect(v2, v1)
    case (vline @ VerticalLine(x1), VerticalLine(x2)) => if (real_close(x1, x2)) vline else NoPoints
    case (_: VerticalLine, _: LineSegment)            => intersect(v2, v1)

    case (seg @ LineSegment(x1, y1, x2, y2), _) => intersect(two_points_to_line(x1, y1, x2, y2), v2) match {
      case NoPoints => NoPoints
      case p @ Point(x0, y0) => {
        def inbetween(v: Double, end1: Double, end2: Double) =
          (end1 - Epsilon <= v && v <= end2 + Epsilon) ||
            (end2 - Epsilon <= v && v <= end1 + Epsilon)
        if (inbetween(x0, x1, x2) && inbetween(y0, y1, y2)) p else NoPoints
      }
      case _: Line         => seg
      case _: VerticalLine => seg
      case seg2: LineSegment =>
        if (real_close(seg.x1, seg.x2)) {
          // the segments are on vertical line
          val (a, b) = if (seg.y1 < seg2.y1) (seg, seg2) else (seg2, seg)
          if (real_close(a.y2, b.y1))
            Point(a.x2, a.y2) // just touching
          else if (a.y2 < b.y1)
            NoPoints // disjoint
          else if (a.y2 > b.y2)
            LineSegment(b.x1, b.y1, b.x2, b.y2) // b inside a
          else
            LineSegment(b.x1, b.y1, a.x2, a.y2) // overlapping
        } else {
          // the segments are on a non-vertical line
          val (a, b) = if (seg.x1 < seg2.x1) (seg, seg2) else (seg2, seg)
          if (real_close(a.x2, b.x1))
            Point(a.x2, a.y2) // just touching
          else if (a.x2 < b.x1)
            NoPoints // disjoint
          else if (a.x2 > b.x2)
            LineSegment(b.x1, b.y1, b.x2, b.y2) // b inside a
          else
            LineSegment(b.x1, b.y1, a.x2, a.y2) // overlapping 
        }
      case _ => throw new Impossible("bad result from intersecting with a line")
    }
    case _ => throw new Impossible("bad call to intersect: only for shape values")
  }
}
