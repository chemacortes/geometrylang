import org.scalatest._
import prop._

import GeometryLang._
import GeometryLang.NoPoints
import GeometryLang.eval_prog.intersect

class SetSpec extends PropSpec with Matchers { //with TableDrivenPropertyChecks with Matchers {

  // Constants for testing
  val ZERO = 0.0
  val ONE = 1.0
  val TWO = 2.0
  val THREE = 3.0
  val FOUR = 4.0
  val FIVE = 5.0
  val SIX = 6.0
  val SEVEN = 7.0
  val TEN = 10.0

  // Point Tests
  val a0 = Point(THREE, FIVE)
  property("Point is initialized properly") {
    a0.x should be(THREE)
    a0.y should be(FIVE)
  }
  property("Point eval_prog return self") {
    eval_prog(a0) shouldBe a0
  }
  property("Point preprocess_prog return self") {
    a0.preprocess_prog shouldBe a0
  }
  val a1 = a0.shift(THREE, FIVE)
  property("Point shift working properly") {
    a1.x should be(SIX)
    a1.y should be(TEN)
  }
  val a2 = eval_prog.intersect(a0, Point(THREE, FIVE))
  val a3 = eval_prog.intersect(a0, Point(FOUR, FIVE))
  property("Point intersect working properly") {
    a2 shouldBe Point(THREE, FIVE)
    a3 should be(NoPoints)
  }

  // Line Tests
  val b = Line(THREE, FIVE)
  property("Line initialized properly") {
    b.m shouldBe THREE
    b.b shouldBe FIVE
  }
  property("Line eval_prog return self") {
    eval_prog(b) shouldBe b
  }
  property("Line preprocess_prog return self") {
    b.preprocess_prog shouldBe b
  }
  val b1 = b.shift(THREE, FIVE)
  property("Line shift working properly") {
    b1.m should be(THREE)
    b1.b should be(ONE)
  }
  val b2 = intersect(b, Line(THREE, FIVE))
  val b3 = intersect(b, Line(THREE, FOUR))
  property("Line intersect working properly") {
    b2 shouldBe Line(THREE, FIVE)
    b3 shouldBe NoPoints
  }

  // VerticalLine Tests
  val c = VerticalLine(THREE)
  property("VerticalLine initialized properly") {
    c.x shouldBe THREE
  }
  property("VerticalLine eval_prog return self") {
    eval_prog(c) shouldBe c
  }
  property("VerticalLine preprocess_prog return self") {
    c.preprocess_prog shouldBe c
  }
  val c1 = c.shift(THREE, FIVE)
  property("VerticalLine shift working properly") {
    c1.x shouldBe SIX
  }
  val c2 = intersect(c, VerticalLine(THREE))
  val c3 = intersect(c, VerticalLine(FOUR))
  property("VerticalLine intersect working properly") {
    c2 shouldBe VerticalLine(THREE)
    c3 shouldBe NoPoints
  }

  //LineSegment Tests
  val d = LineSegment(ONE, TWO, -THREE, -FOUR)
  property("LineSegment eval_prog return self") {
    eval_prog(d) shouldBe d
  }
  val d1 = LineSegment(ONE, TWO, ONE, TWO)
  val d2 = d1.preprocess_prog
  property("LineSegment preprocess_prog convert to a Point " +
    "if ends of segment are real_close") {
    d2 shouldBe Point(ONE, TWO)
  }
  val dp = d.preprocess_prog
  property("LineSegment preprocess_prog should make x1 and y1 " +
    "on the left of x2 and y2") {
    dp shouldBe LineSegment(-THREE, -FOUR, ONE, TWO)
  }
  val d3 = dp.shift(THREE, FIVE)
  property("LineSegment shift working properly") {
    d3 shouldBe LineSegment(ZERO, ONE, FOUR, SEVEN)
  }
  val d4 = intersect(dp, LineSegment(-THREE, -FOUR, ONE, TWO))
  val d5 = intersect(dp, LineSegment(TWO, THREE, FOUR, FIVE))
  property("LineSegment intersect working properly") {
    d4 shouldBe LineSegment(-THREE, -FOUR, ONE, TWO)
    d5 shouldBe NoPoints
  }

  //Intersect Tests
  val i = Intersect(LineSegment(-ONE, -TWO, THREE, FOUR),
    LineSegment(THREE, FOUR, -ONE, -TWO))
  val i1 = eval_prog(i.preprocess_prog)
  property("Intersect eval_prog return the intersect between e1 and e2") {
    i1 shouldBe LineSegment(-ONE, -TWO, THREE, FOUR)
  }

  //Var Tests
  val v = Var("a")
  val v1 = eval_prog(v, List(("a" -> Point(THREE, FIVE))))
  property("Var eval_prog is working properly") {
    v1 shouldBe Point(THREE, FIVE)
  }
  property("Var preprocess_prog return self") {
    v1.preprocess_prog shouldBe v1
  }

  //Let Tests
  val l = Let("a", LineSegment(-ONE, -TWO, THREE, FOUR),
    Intersect(Var("a"), LineSegment(THREE, FOUR, -ONE, -TWO)))
  val l1 = eval_prog(l.preprocess_prog)
  property("Let eval_prog evaluate e2 after adding [s, e1] to the environment") {
    l1 shouldBe LineSegment(-ONE, -TWO, THREE, FOUR)
  }

  //Let Variable Shadowing Test
  val l2 = Let("a", LineSegment(-ONE, -TWO, THREE, FOUR),
    Let("b", LineSegment(THREE, FOUR, -ONE, -TWO),
      Intersect(Var("a"), Var("b"))))
  val l2e = eval_prog(l2.preprocess_prog, List(("a" -> Point(0, 0))))
  property("Let eval_prog evaluate e2 after adding [s, e1] to the environment (shadowing)") {
    l2e shouldBe LineSegment(-ONE, -TWO, THREE, FOUR)
  }

  //Shift Tests
  val s = Shift(THREE, FIVE, LineSegment(-ONE, -TWO, THREE, FOUR))
  val s1 = eval_prog(s.preprocess_prog)
  property("Shift shift e by dx and dy") {
    s1 shouldBe LineSegment(TWO, THREE, SIX, 9)
  }

  //Some grader tests around intersect methods
  val g = Intersect(Point(2.5, 1.5),
    Intersect(LineSegment(2.0, 1.0, 3.0, 2.0),
      Intersect(LineSegment(0.0, 0.0, 2.5, 1.5),
        Line(1.0, -1.0))))
  val g1 = eval_prog(g.preprocess_prog)
  property("Intersect#eval_prog") {
    g1 shouldBe Point(2.5, 1.5)
  }
  property("Line#intersect") {
    intersect(Line(5.0, 2.0), Line(7.0, -1.0)) shouldBe Point(1.5, 9.5)
    intersect(Line(5.0, 0.0), LineSegment(1.0, 5.0, 2.0, 2.0)) shouldBe Point(1.0, 5.0)
    intersect(Line(5.0, 0.0), LineSegment(-1.0, -1.0, 1.0, 5.0)) shouldBe Point(1.0, 5.0)
  }
  property("LineSegment#intersect") {
    intersect(LineSegment(1.0, 5.0, 2.0, 2.0), Line(5.0, 0.0)) shouldBe Point(1.0, 5.0)
    intersect(LineSegment(-1.0, -1.0, 1.0, 5.0), Line(5.0, 0.0)) shouldBe Point(1.0, 5.0)
    intersect(LineSegment(5.0, 7.0, 9.0, 9.0), LineSegment(5.0, 7.0, 6.0, -1.0)) shouldBe Point(5.0, 7.0)
    intersect(LineSegment(5.0, 2.0, 9.0, 9.0), LineSegment(-2.0, -1.0, 5.0, 2.0)) shouldBe Point(5.0, 2.0)
    intersect(LineSegment(2.0, 3.0, 8.0, 4.0), LineSegment(1.0, 1.0, 8.0, 4.0)) shouldBe Point(8.0, 4.0)
  }

}
